package com.example.demodocker.controller;

import com.example.demodocker.entity.User;
import com.example.demodocker.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {
    private final UserService userService;

    @PostMapping
    public ResponseEntity<?> create(String name){
        User user = userService.create(name);
        return ResponseEntity.ok(user);
    }
    //my teammate add feature 1
//    I add feature
//    I add feature 2
//  I add feature 3
    // I add f4
    // teammate add f 2
    // add 2
}
