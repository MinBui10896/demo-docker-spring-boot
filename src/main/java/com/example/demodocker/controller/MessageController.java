package com.example.demodocker.controller;

import com.example.demodocker.entity.Message;
import com.example.demodocker.entity.User;
import com.example.demodocker.service.MessageService;
import com.example.demodocker.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessageController {
    private final MessageService messageService;

    @PostMapping
    public ResponseEntity<?> create(String name, String msg){
        Message message = messageService.create(name, msg);
        return ResponseEntity.ok(message);
    }
    //add 1
    //team 1
    //team 2
}
