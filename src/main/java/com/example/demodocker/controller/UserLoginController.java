package com.example.demodocker.controller;

import com.example.demodocker.entity.User;
import com.example.demodocker.entity.UserLogin;
import com.example.demodocker.service.UserLoginService;
import com.example.demodocker.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user-login")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserLoginController {
    private final UserLoginService userLoginService;

    @PostMapping
    public ResponseEntity<?> create(String name){
        UserLogin userLogin = userLoginService.create(name);
        return ResponseEntity.ok(userLogin);
    }
}
