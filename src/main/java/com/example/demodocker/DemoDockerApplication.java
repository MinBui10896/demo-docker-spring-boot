package com.example.demodocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EntityScan(basePackages = {"com.example.demodocker"})
@SpringBootApplication
@EnableMongoRepositories("com.example") //to activate MongoDB repositories
public class DemoDockerApplication {

    @RequestMapping("/hello")
    public String helloDocker() {
        return "Hello Docker!";
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoDockerApplication.class, args);
    }

}
