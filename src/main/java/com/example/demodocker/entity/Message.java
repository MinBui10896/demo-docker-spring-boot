package com.example.demodocker.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

@Data
@Document(collection = "message")
public class Message {
    @Id
    private String id;

    private String name;

    private String message;
}
