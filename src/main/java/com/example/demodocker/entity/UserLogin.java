package com.example.demodocker.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("UserLgin")
@Data
public class UserLogin implements Serializable {
    @Id
    private Long id;

    private String name;

}
