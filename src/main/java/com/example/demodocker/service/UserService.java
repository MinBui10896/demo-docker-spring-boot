package com.example.demodocker.service;

import com.example.demodocker.entity.User;
import com.example.demodocker.repo.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {
    private final UserRepository userRepository;

    public User create(String name){
        User user = new User();
        user.setName(name);
        return userRepository.save(user);
    }
}
