package com.example.demodocker.service;

import com.example.demodocker.entity.Message;
import com.example.demodocker.entity.User;
import com.example.demodocker.repo.MessageRepository;
import com.example.demodocker.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessageService {
    private final MessageRepository messageRepository;

    public Message create(String name, String msg){
        Message message = new Message();
        message.setName(name);
        message.setMessage(msg);
        return messageRepository.save(message);
    }
}
