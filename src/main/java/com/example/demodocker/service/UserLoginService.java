package com.example.demodocker.service;

import com.example.demodocker.entity.User;
import com.example.demodocker.entity.UserLogin;
import com.example.demodocker.repo.UserLoginRepository;
import com.example.demodocker.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserLoginService {
    private final UserLoginRepository userLoginRepository;

    public UserLogin create(String name){
        UserLogin userLogin = new UserLogin();
        userLogin.setName(name);
        return userLoginRepository.save(userLogin);
    }
}
