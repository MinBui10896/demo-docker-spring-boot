package com.example.demodocker.repo;

import com.example.demodocker.entity.User;
import com.example.demodocker.entity.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginRepository extends CrudRepository<UserLogin, Long> {

}
